# Changelog

All notable changes to this project will be documented in this file.

## [0.1.2] - 2024-10-22

### 🐛 Bug Fixes

- *(deps)* Update module github.com/hashicorp/go-plugin to v1.6.2
- *(deps)* Update module gitlab.com/unboundsoftware/sloth/model to v0.1.2

### ⚙️ Miscellaneous Tasks

- Use custom release notes
- Add CHANGES.md and VERSION files to .gitignore

## [0.1.1] - 2024-10-17

### 🐛 Bug Fixes

- *(deps)* Update module github.com/stretchr/testify to v1.9.0
- *(deps)* Update module github.com/go-redis/redis/v8 to v9
- *(deps)* Update module github.com/alecthomas/kong to v0.9.0
- *(deps)* Update module github.com/alicebob/miniredis/v2 to v2.32.1
- *(deps)* Update module github.com/hashicorp/go-hclog to v1.6.3
- *(deps)* Update module github.com/hashicorp/go-plugin to v1.6.1
- *(deps)* Update module github.com/alicebob/miniredis/v2 to v2.33.0
- *(deps)* Update module github.com/redis/go-redis/v9 to v9.5.2
- *(deps)* Update module github.com/redis/go-redis/v9 to v9.5.3
- *(deps)* Update module github.com/redis/go-redis/v9 to v9.5.4
- *(deps)* Update module github.com/redis/go-redis/v9 to v9.6.0
- *(deps)* Update module github.com/redis/go-redis/v9 to v9.6.1
- *(deps)* Update module github.com/alecthomas/kong to v1
- *(deps)* Update module github.com/alecthomas/kong to v1.2.0
- *(deps)* Update module github.com/alecthomas/kong to v1.2.1
- *(deps)* Update module github.com/redis/go-redis/v9 to v9.6.2
- *(deps)* Update module gitlab.com/unboundsoftware/sloth/model to v0.1.1
- *(deps)* Update module github.com/redis/go-redis/v9 to v9.7.0

### 💼 Other

- *(deps)* Bump github.com/alecthomas/kong from 0.8.0 to 0.8.1
- *(deps)* Bump github.com/hashicorp/go-hclog from 0.14.1 to 1.5.0
- *(deps)* Bump github.com/google/uuid from 1.3.1 to 1.4.0
- *(deps)* Bump github.com/hashicorp/go-plugin from 1.5.2 to 1.6.0
- *(deps)* Bump github.com/hashicorp/go-hclog from 1.5.0 to 1.6.0
- *(deps)* Bump github.com/hashicorp/go-hclog from 1.6.0 to 1.6.1
- *(deps)* Bump github.com/google/uuid from 1.4.0 to 1.5.0
- *(deps)* Bump github.com/hashicorp/go-hclog from 1.6.1 to 1.6.2
- *(deps)* Bump github.com/alicebob/miniredis/v2 from 2.31.0 to 2.31.1
- *(deps)* Bump github.com/google/uuid from 1.5.0 to 1.6.0

### ⚙️ Miscellaneous Tasks

- Update goreleaser image version
- Add Unbound Release in tag-only mode

### 🛡️ Security

- *(deps)* [security] bump golang.org/x/net from 0.7.0 to 0.17.0
- *(deps)* [security] bump google.golang.org/grpc

## [0.1.0] - 2023-10-12

### 🚀 Features

- Move to hashicorp/go-plugin

### 💼 Other

- *(deps)* Bump github.com/stretchr/testify from 1.8.0 to 1.8.1
- *(deps)* Bump github.com/alicebob/miniredis/v2 from 2.23.0 to 2.23.1
- *(deps)* Bump github.com/alicebob/miniredis/v2 from 2.23.1 to 2.30.0
- *(deps)* Bump github.com/stretchr/testify from 1.8.1 to 1.8.2
- *(deps)* Bump github.com/alicebob/miniredis/v2 from 2.30.0 to 2.30.1
- *(deps)* Bump github.com/alicebob/miniredis/v2 from 2.30.1 to 2.30.2
- *(deps)* Bump github.com/stretchr/testify from 1.8.2 to 1.8.3
- *(deps)* Bump github.com/stretchr/testify from 1.8.3 to 1.8.4
- *(deps)* Bump github.com/alicebob/miniredis/v2 from 2.30.2 to 2.30.3
- *(deps)* Bump github.com/alicebob/miniredis/v2 from 2.30.3 to 2.30.4
- *(deps)* Bump github.com/alicebob/miniredis/v2 from 2.30.4 to 2.30.5
- *(deps)* Bump github.com/google/uuid from 1.3.0 to 1.3.1
- *(deps)* Bump github.com/alicebob/miniredis/v2 from 2.30.5 to 2.31.0

### ⚙️ Miscellaneous Tasks

- Use Docker DinD version from variable
- Switch to manual rebases for Dependabot
- Update to Go 1.20
- Check goreleaser config and do a dry run

## [0.0.12] - 2022-08-12

### 💼 Other

- *(deps)* Bump gitlab.com/unboundsoftware/sloth/model
- *(deps)* Bump github.com/alicebob/miniredis/v2 from 2.22.0 to 2.23.0

## [0.0.11] - 2022-06-30

### 🐛 Bug Fixes

- Use logger directly

### 💼 Other

- *(deps)* Bump github.com/stretchr/testify from 1.7.1 to 1.7.2
- *(deps)* Bump gitlab.com/unboundsoftware/apex-mocks
- *(deps)* Bump gitlab.com/unboundsoftware/apex-mocks
- *(deps)* Bump github.com/stretchr/testify from 1.7.2 to 1.7.3
- *(deps)* Bump github.com/stretchr/testify from 1.7.3 to 1.7.4
- *(deps)* Bump github.com/stretchr/testify from 1.7.4 to 1.7.5
- *(deps)* Bump github.com/stretchr/testify from 1.7.5 to 1.8.0
- *(deps)* Bump github.com/alicebob/miniredis/v2 from 2.21.0 to 2.22.0

## [0.0.10] - 2022-05-05

### 🐛 Bug Fixes

- Replace channels with context.WithCancel
- Use time.Ticker instead of time.Sleep

### 💼 Other

- *(deps)* Bump github.com/alicebob/miniredis/v2 from 2.17.0 to 2.18.0
- *(deps)* Bump github.com/sanity-io/litter from 1.5.2 to 1.5.4
- *(deps)* Bump github.com/stretchr/testify from 1.7.0 to 1.7.1
- Update to Go 1.18
- *(deps)* Bump github.com/alicebob/miniredis/v2 from 2.18.0 to 2.19.0
- *(deps)* Bump github.com/go-redis/redis/v8 from 8.11.4 to 8.11.5
- *(deps)* Bump github.com/alicebob/miniredis/v2 from 2.19.0 to 2.20.0
- *(deps)* Bump github.com/sanity-io/litter from 1.5.4 to 1.5.5
- *(deps)* Bump github.com/alicebob/miniredis/v2 from 2.20.0 to 2.21.0

### ⚙️ Miscellaneous Tasks

- Add dependabot config
- Change to codecov binary instead of bash uploader
- Reduce timeout of tests

## [0.0.9] - 2021-07-22

### ⚙️ Miscellaneous Tasks

- Update to latest version of goreleaser

## [0.0.8] - 2021-04-07

### 🐛 Bug Fixes

- Set started to false on error

## [0.0.7] - 2021-04-01

### 🐛 Bug Fixes

- Handle unstarted in stop

### ⚙️ Miscellaneous Tasks

- Upgrade goreleaser to fix Gitlab upload issue

## [0.0.6] - 2021-03-10

### 🐛 Bug Fixes

- Initialize done-chan to make shutdown work

## [0.0.5] - 2021-03-02

### 🐛 Bug Fixes

- Ignore coverage-files

### ⚙️ Miscellaneous Tasks

- Add tests
- Set fixed version of goreleaser

## [0.0.4] - 2021-01-14

### ⚙️ Miscellaneous Tasks

- Add ENV-config

## [0.0.3] - 2021-01-14

### 🐛 Bug Fixes

- Enable CGO

## [0.0.2] - 2021-01-13

### 🚀 Features

- Initial commit

<!-- generated by git-cliff -->
