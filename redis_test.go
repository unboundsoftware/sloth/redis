/*
 * MIT License
 *
 * Copyright (c) 2021 Unbound Software Development Svenska AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package main

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"reflect"
	"strings"
	"testing"
	"time"

	"github.com/alicebob/miniredis/v2"
	"github.com/google/uuid"
	"github.com/hashicorp/go-hclog"
	"github.com/redis/go-redis/v9"
	"github.com/sanity-io/litter"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"gitlab.com/unboundsoftware/sloth/model"
)

func TestRedis_Start(t *testing.T) {
	type args struct {
		handler func(model.Request) error
		quit    chan error
	}
	tests := []struct {
		name       string
		mock       *mockClient
		args       args
		quit       bool
		wantErr    bool
		wantLogged []string
	}{
		{
			name: "error fetching zrange",
			args: args{
				quit: make(chan error, 10),
			},
			mock: &mockClient{
				zrange: func(ctx context.Context, key string, start, stop int64) *redis.ZSliceCmd {
					r := &redis.ZSliceCmd{}
					r.SetErr(errors.New("error"))
					return r
				},
			},
			wantErr:    false,
			wantLogged: []string{"[INFO]  got quit-signal, exiting\n"},
		},
		{
			name:       "quit signal",
			args:       args{},
			quit:       true,
			wantErr:    false,
			wantLogged: []string{"[INFO]  got quit-signal, exiting\n"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logged := &bytes.Buffer{}
			logger := hclog.New(&hclog.LoggerOptions{Output: logged, DisableTime: true})
			s, err := miniredis.Run()
			require.NoError(t, err)
			defer s.Close()
			quit := make(chan bool, 10)
			if tt.quit {
				quit <- true
			}
			var client Client = redis.NewClient(&redis.Options{
				Addr: s.Addr(),
				DB:   0,
			})
			if tt.mock != nil {
				tt.mock.client = client
				client = tt.mock
			}
			if tt.args.quit != nil {
				go func() {
					<-tt.args.quit
					quit <- true
				}()
			}
			r := &Redis{
				client: client,
				quit:   quit,
				timeSource: func() time.Time {
					return time.Date(2021, 2, 2, 19, 40, 12, 123456789, time.UTC)
				},
				sleep:  func(duration time.Duration) {},
				logger: logger,
			}
			if err := r.Start(model.HandlerFunc(tt.args.handler), model.ChannelQuitter(tt.args.quit)); (err != nil) != tt.wantErr {
				t.Errorf("Start() error = %v, wantErr %v", err, tt.wantErr)
			}
			<-r.ctx.Done()
			assert.Equal(t, strings.Join(tt.wantLogged, "\n"), logged.String())
		})
	}
}

func TestRedis_process(t *testing.T) {
	type fields struct {
		client     Client
		quit       chan bool
		timeSource func() time.Time
		sleep      func(duration time.Duration)
	}
	type args struct {
		handler func(model.Request) error
	}
	tests := []struct {
		name       string
		setup      func(redis *miniredis.Miniredis)
		fields     fields
		args       args
		wantErr    bool
		wantLogged []string
	}{
		{
			name: "no requests",
			setup: func(redis *miniredis.Miniredis) {
				_, _ = redis.ZAdd("sloth:", 0, "x")
				_, _ = redis.ZRem("sloth:", "x")
			},
		},
		{
			name: "error unmarshaling",
			setup: func(redis *miniredis.Miniredis) {
				_, _ = redis.ZAdd("sloth:", 0, `{"a":`)
			},
			wantErr: true,
		},
		{
			name: "scheduled in future",
			setup: func(redis *miniredis.Miniredis) {
				_, _ = redis.ZAdd("sloth:", 0, `{"Req":{"DelayedUntil":"2021-02-02T20:00:00.000Z"}}`)
			},
		},
		{
			name: "error handling",
			setup: func(redis *miniredis.Miniredis) {
				buff, _ := json.Marshal(&stored{
					ID: "1",
					Req: model.Request{
						DelayedUntil: time.Date(2021, 2, 2, 19, 40, 0, 0, time.UTC),
						Target:       "test://host/path",
						Payload:      json.RawMessage("{\"a\":\"b\"}"),
					},
				})
				_, _ = redis.ZAdd("sloth:", 0, string(buff))
			},
			args: args{
				handler: func(request model.Request) error {
					want := model.Request{
						DelayedUntil: time.Date(2021, 2, 2, 19, 40, 0, 0, time.UTC),
						Target:       "test://host/path",
						Payload:      json.RawMessage("{\"a\":\"b\"}"),
					}
					if !reflect.DeepEqual(request, want) {
						t.Errorf("process() got %s, want %s", litter.Sdump(request), litter.Sdump(want))
					}
					return errors.New("error")
				},
			},
			wantErr: true,
			wantLogged: []string{
				"[ERROR] error handling request: error=error\n",
			},
		},
		{
			name: "success",
			setup: func(redis *miniredis.Miniredis) {
				buff, _ := json.Marshal(&stored{
					ID: "1",
					Req: model.Request{
						DelayedUntil: time.Date(2021, 2, 2, 19, 40, 0, 0, time.UTC),
						Target:       "test://host/path",
						Payload:      json.RawMessage("{\"a\":\"b\"}"),
					},
				})
				_, _ = redis.ZAdd("sloth:", 0, string(buff))
			},
			args: args{
				handler: func(request model.Request) error {
					return nil
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logged := &bytes.Buffer{}
			logger := hclog.New(&hclog.LoggerOptions{Output: logged, DisableTime: true})
			s, err := miniredis.Run()
			if err != nil {
				panic(err)
			}
			defer s.Close()
			if tt.setup != nil {
				tt.setup(s)
			}
			quit := make(chan bool, 10)
			r := &Redis{
				client: redis.NewClient(&redis.Options{
					Addr: s.Addr(),
					DB:   0,
				}),
				quit: quit,
				timeSource: func() time.Time {
					return time.Date(2021, 2, 2, 19, 40, 12, 123456789, time.UTC)
				},
				sleep:  func(duration time.Duration) {},
				logger: logger,
			}
			err = r.process(tt.args.handler)
			if (err != nil) != tt.wantErr {
				t.Errorf("process() error = %v, wantErr %v", err, tt.wantErr)
			}
			assert.Equal(t, strings.Join(tt.wantLogged, "\n"), logged.String())
		})
	}
}

func TestRedis_handle(t *testing.T) {
	type args struct {
		request *stored
		handler func(model.Request) error
		member  *redis.Z
	}
	tests := []struct {
		name       string
		setup      func(redis *miniredis.Miniredis)
		mock       *mockClient
		args       args
		wantErr    bool
		wantLogged []string
	}{
		{
			name: "error locking",
			setup: func(redis *miniredis.Miniredis) {
				_ = redis.Set("lock:1", "abc")
			},
			args: args{
				request: &stored{ID: "1"},
			},
			wantErr: true,
		},
		{
			name: "error handling",
			setup: func(redis *miniredis.Miniredis) {
			},
			args: args{
				request: &stored{ID: "1", Req: model.Request{
					DelayedUntil: time.Date(2021, 2, 3, 18, 56, 1, 0, time.UTC),
					Target:       "test://host/path",
					Payload:      json.RawMessage("{}"),
				}},
				handler: func(request model.Request) error {
					want := model.Request{
						DelayedUntil: time.Date(2021, 2, 3, 18, 56, 1, 0, time.UTC),
						Target:       "test://host/path",
						Payload:      json.RawMessage("{}"),
					}

					if !reflect.DeepEqual(request, want) {
						t.Errorf("handle() got %s, want %s", litter.Sdump(request), litter.Sdump(want))
					}
					return errors.New("error")
				},
			},
			wantErr:    true,
			wantLogged: []string{"[ERROR] error handling request: error=error\n"},
		},
		{
			name: "error unlocking",
			mock: &mockClient{
				del: func(ctx context.Context, keys ...string) *redis.IntCmd {
					r := &redis.IntCmd{}
					r.SetErr(errors.New("error"))
					return r
				},
			},
			args: args{
				request: &stored{ID: "1"},
				handler: func(request model.Request) error {
					return errors.New("error")
				},
			},
			wantErr: true,
			wantLogged: []string{
				"[ERROR] error handling request: error=error",
				"[ERROR] unable to release lock: id=1\n",
			},
		},
		{
			name: "error acking",
			mock: &mockClient{
				zrem: func(ctx context.Context, key string, members ...interface{}) *redis.IntCmd {
					r := &redis.IntCmd{}
					r.SetErr(errors.New("error"))
					return r
				},
			},
			args: args{
				request: &stored{ID: "1"},
				member:  &redis.Z{Member: "a"},
				handler: func(request model.Request) error {
					return nil
				},
			},
			wantErr: true,
			wantLogged: []string{
				"[ERROR] unable to remove request: error=error",
				"[ERROR] unable to acknowledge request: id=1\n",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logged := &bytes.Buffer{}
			logger := hclog.New(&hclog.LoggerOptions{Output: logged, DisableTime: true})
			s, err := miniredis.Run()
			if err != nil {
				panic(err)
			}
			defer s.Close()
			if tt.setup != nil {
				tt.setup(s)
			}
			var client Client = redis.NewClient(&redis.Options{
				Addr: s.Addr(),
				DB:   0,
			})
			if tt.mock != nil {
				tt.mock.client = client
				client = tt.mock
			}
			r := &Redis{
				client:     client,
				timeSource: time.Now,
				sleep:      time.Sleep,
				logger:     logger,
			}
			if err := r.handle(tt.args.request, tt.args.handler, tt.args.member); (err != nil) != tt.wantErr {
				t.Errorf("handle() error = %v, wantErr %v", err, tt.wantErr)
			}
			time.Sleep(100 * time.Millisecond)
			assert.Equal(t, strings.Join(tt.wantLogged, "\n"), logged.String())
		})
	}
}

func TestRedis_Add(t *testing.T) {
	type args struct {
		request model.Request
	}
	tests := []struct {
		name    string
		setup   func(redis *miniredis.Miniredis)
		args    args
		wantErr bool
		want    []string
	}{
		{
			name: "error marshaling",
			args: args{request: model.Request{
				DelayedUntil: time.Date(2021, 2, 3, 18, 56, 1, 0, time.UTC),
				Target:       "test://host/path",
				Payload:      json.RawMessage{},
			}},
			wantErr: true,
		},
		{
			name: "error storing",
			setup: func(redis *miniredis.Miniredis) {
				redis.SetError("error")
			},
			args:    args{},
			wantErr: true,
		},
		{
			name: "success",
			args: args{request: model.Request{
				DelayedUntil: time.Date(2021, 2, 3, 18, 56, 1, 0, time.UTC),
				Target:       "test://host/path",
				Payload:      json.RawMessage("{}"),
			}},
			wantErr: false,
			want:    []string{`{"ID":"61626364-6566-4768-a96a-6b6c6d6e6f70","Req":{"DelayedUntil":"2021-02-03T18:56:01Z","Target":"test://host/path","Payload":{}}}`},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			uuid.SetRand(strings.NewReader("abcdefghijklmnopqrstuvwxyz"))
			s, err := miniredis.Run()
			if err != nil {
				panic(err)
			}
			defer s.Close()
			if tt.setup != nil {
				tt.setup(s)
			}
			r := &Redis{
				client: redis.NewClient(&redis.Options{
					Addr: s.Addr(),
					DB:   0,
				}),
				timeSource: time.Now,
				sleep:      time.Sleep,
			}
			if err := r.Add(tt.args.request); (err != nil) != tt.wantErr {
				t.Errorf("Add() error = %v, wantErr %v", err, tt.wantErr)
			}
			members, _ := s.ZMembers("sloth:")
			if !reflect.DeepEqual(tt.want, members) {
				t.Errorf("Add() got %s, want %s", litter.Sdump(members), litter.Sdump(tt.want))
			}
		})
	}
}

func TestRedis_remove(t *testing.T) {
	type args struct {
		member *redis.Z
	}
	tests := []struct {
		name       string
		setup      func(redis *miniredis.Miniredis)
		args       args
		wantErr    bool
		wantLogged []string
	}{
		{
			name: "error",
			setup: func(redis *miniredis.Miniredis) {
				redis.SetError("error")
			},
			args:       args{member: &redis.Z{Member: "a"}},
			wantErr:    true,
			wantLogged: []string{"[ERROR] unable to remove request: error=error\n"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logged := &bytes.Buffer{}
			logger := hclog.New(&hclog.LoggerOptions{Output: logged, DisableTime: true})
			s, err := miniredis.Run()
			if err != nil {
				panic(err)
			}
			defer s.Close()
			if tt.setup != nil {
				tt.setup(s)
			}
			r := &Redis{
				client: redis.NewClient(&redis.Options{
					Addr: s.Addr(),
					DB:   0,
				}),
				timeSource: time.Now,
				sleep:      time.Sleep,
				logger:     logger,
			}
			if err := r.remove(tt.args.member); (err != nil) != tt.wantErr {
				t.Errorf("remove() error = %v, wantErr %v", err, tt.wantErr)
			}
			assert.Equal(t, strings.Join(tt.wantLogged, "\n"), logged.String())
		})
	}
}

func TestRedis_lock(t *testing.T) {
	type args struct {
		id string
	}
	tests := []struct {
		name    string
		setup   func(redis *miniredis.Miniredis)
		args    args
		want    string
		wantErr bool
	}{
		{
			name: "error",
			setup: func(redis *miniredis.Miniredis) {
				redis.SetError("error")
			},
			args:    args{},
			want:    "",
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			uuid.SetRand(strings.NewReader("abcdefghijklmnopqrstuvwxyz"))
			s, err := miniredis.Run()
			if err != nil {
				panic(err)
			}
			defer s.Close()
			if tt.setup != nil {
				tt.setup(s)
			}
			r := &Redis{
				client: redis.NewClient(&redis.Options{
					Addr: s.Addr(),
					DB:   0,
				}),
				timeSource: time.Now,
				sleep:      time.Sleep,
			}
			got, err := r.lock(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("lock() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("lock() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestRedis_Stop(t *testing.T) {
	tests := []struct {
		name       string
		started    bool
		wantErr    bool
		wantLogged []string
	}{
		{
			name:       "not started",
			wantErr:    false,
			wantLogged: []string{"[INFO]  stopping\n"},
		},
		{
			name:       "started",
			started:    true,
			wantErr:    false,
			wantLogged: []string{"[INFO]  stopping\n"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logged := &bytes.Buffer{}
			logger := hclog.New(&hclog.LoggerOptions{Output: logged, DisableTime: true})
			s, err := miniredis.Run()
			if err != nil {
				panic(err)
			}
			defer s.Close()
			ctx, cancel := context.WithCancel(context.Background())
			r := &Redis{
				client: redis.NewClient(&redis.Options{
					Addr: s.Addr(),
					DB:   0,
				}),
				quit:   make(chan bool, 1),
				logger: logger,
				ctx:    ctx,
				cancel: cancel,
			}
			if err := r.Stop(); (err != nil) != tt.wantErr {
				t.Errorf("Stop() error = %v, wantErr %v", err, tt.wantErr)
			}
			assert.Equal(t, strings.Join(tt.wantLogged, "\n"), logged.String())
		})
	}
}

func TestRedis_Configure(t *testing.T) {
	tests := []struct {
		name       string
		args       []string
		want       bool
		wantErr    assert.ErrorAssertionFunc
		wantLogged []string
	}{
		{
			name: "invalid parameter",
			args: []string{"--invalid"},
			want: false,
			wantErr: func(t assert.TestingT, err error, i ...interface{}) bool {
				return assert.EqualError(t, err, "unknown flag --invalid")
			},
			wantLogged: nil,
		},
		{
			name:       "no parameters",
			args:       []string{},
			want:       false,
			wantErr:    assert.NoError,
			wantLogged: nil,
		},
		{
			name:       "success",
			args:       []string{"--redis-address", "localhost:6379"},
			want:       true,
			wantErr:    assert.NoError,
			wantLogged: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			logged := &bytes.Buffer{}
			logger := hclog.New(&hclog.LoggerOptions{Output: logged, DisableTime: true})
			r := &Redis{
				logger: logger,
			}
			got, err := r.Configure(tt.args)
			if !tt.wantErr(t, err, fmt.Sprintf("Configure(%v)", tt.args)) {
				return
			}
			assert.Equalf(t, tt.want, got, "Configure(%v)", tt.args)
			assert.Equal(t, strings.Join(tt.wantLogged, "\n"), logged.String())
		})
	}
}

func TestRedis_IsSource(t *testing.T) {
	assert.Equalf(t, false, (&Redis{}).IsSource(), "IsSource()")
}

func TestRedis_IsSink(t *testing.T) {
	assert.Equalf(t, false, (&Redis{}).IsSink(), "IsSink()")
}

func TestRedis_IsStore(t *testing.T) {
	assert.Equalf(t, true, (&Redis{}).IsStore(), "IsStore()")
}

type mockClient struct {
	client Client
	del    func(ctx context.Context, keys ...string) *redis.IntCmd
	zrem   func(ctx context.Context, key string, members ...interface{}) *redis.IntCmd
	zrange func(ctx context.Context, key string, start, stop int64) *redis.ZSliceCmd
}

func (m mockClient) ZRem(ctx context.Context, key string, members ...interface{}) *redis.IntCmd {
	if m.zrem != nil {
		return m.zrem(ctx, key, members...)
	}
	return m.client.ZRem(ctx, key, members...)
}

func (m mockClient) SetNX(ctx context.Context, key string, value interface{}, expiration time.Duration) *redis.BoolCmd {
	return m.client.SetNX(ctx, key, value, expiration)
}

func (m mockClient) Del(ctx context.Context, keys ...string) *redis.IntCmd {
	if m.del != nil {
		return m.del(ctx, keys...)
	}
	return m.client.Del(ctx, keys...)
}

func (m mockClient) ZRangeWithScores(ctx context.Context, key string, start, stop int64) *redis.ZSliceCmd {
	if m.zrange != nil {
		return m.zrange(ctx, key, start, stop)
	}
	return m.client.ZRangeWithScores(ctx, key, start, stop)
}

func (m mockClient) ZAdd(context.Context, string, ...redis.Z) *redis.IntCmd {
	panic("implement me")
}

func (m mockClient) Close() error {
	return nil
}

var _ Client = &mockClient{}
