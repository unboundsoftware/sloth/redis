/*
 * MIT License
 *
 * Copyright (c) 2021 Unbound Software Development Svenska AB
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */

package main

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"sync"
	"time"

	"github.com/alecthomas/kong"
	"github.com/google/uuid"
	"github.com/hashicorp/go-hclog"
	"github.com/hashicorp/go-plugin"
	"github.com/redis/go-redis/v9"
	"gitlab.com/unboundsoftware/sloth/model"
	mp "gitlab.com/unboundsoftware/sloth/model/plugin"
)

type Config struct {
	RedisAddress  string `name:"redis-address" env:"REDIS_ADDRESS" help:"Address (including port) to Redis-server"`
	RedisUsername string `name:"redis-username" env:"REDIS_USERNAME" help:"Redis username"`
	RedisPassword string `name:"redis-password" env:"REDIS_PASSWORD" help:"Redis password"`
}

type Client interface {
	ZAdd(ctx context.Context, key string, members ...redis.Z) *redis.IntCmd
	ZRangeWithScores(ctx context.Context, key string, start, stop int64) *redis.ZSliceCmd
	ZRem(ctx context.Context, key string, members ...interface{}) *redis.IntCmd
	SetNX(ctx context.Context, key string, value interface{}, expiration time.Duration) *redis.BoolCmd
	Del(ctx context.Context, keys ...string) *redis.IntCmd
	Close() error
}

type Redis struct {
	client     Client
	quit       chan bool
	ctx        context.Context
	cancel     context.CancelFunc
	timeSource func() time.Time
	sleep      func(duration time.Duration)
	logger     hclog.Logger
	mutex      sync.Mutex
}

func (r *Redis) Configure(args []string) (bool, error) {
	cli := &Config{}
	cmd, err := kong.New(
		cli,
		kong.Description("redis_store is a pluggable store for sloth"),
		kong.Writers(os.Stdout, os.Stdin),
	)
	if err != nil {
		return false, err
	}
	_, err = cmd.Parse(args)
	if err != nil {
		return false, err
	}

	if cli.RedisAddress == "" {
		return false, nil
	}

	rdb := redis.NewClient(&redis.Options{
		Addr:     cli.RedisAddress,
		Username: cli.RedisUsername,
		Password: cli.RedisPassword,
		DB:       0,
	})
	r.client = rdb
	r.quit = make(chan bool, 2)
	r.timeSource = time.Now
	r.sleep = time.Sleep

	return true, nil
}

func (r *Redis) IsSource() bool {
	return false
}

func (r *Redis) IsSink() bool {
	return false
}

func (r *Redis) IsStore() bool {
	return true
}

func (r *Redis) Start(handler model.Handler, quitter model.Quitter) error {
	r.mutex.Lock()
	defer r.mutex.Unlock()
	go r.loop(handler.Handle, quitter)
	ctx, cancel := context.WithCancel(context.Background())
	r.ctx = ctx
	r.cancel = cancel
	return nil
}

func (r *Redis) loop(handler func(model.Request) error, quitter model.Quitter) {
	defer func() {
		r.mutex.Lock()
		defer r.mutex.Unlock()
		r.cancel()
	}()
	ticker := time.NewTicker(time.Second)
	for {
		select {
		case <-r.quit:
			r.logger.Info("got quit-signal, exiting")
			return
		case <-ticker.C:
			if err := r.process(handler); err != nil {
				quitter.Quit(err)
				break
			}
		}
	}
}

func (r *Redis) process(handler func(model.Request) error) error {
	r.mutex.Lock()
	defer r.mutex.Unlock()
	values, err := r.client.ZRangeWithScores(context.Background(), "sloth:", 0, 0).Result()
	if err != nil {
		return fmt.Errorf("redis zrange error: %w", err)
	}
	if len(values) != 0 {
		for _, v := range values {
			var request *stored
			err := json.Unmarshal([]byte(v.Member.(string)), &request)
			if err != nil {
				return err
			}
			if !request.Req.DelayedUntil.After(r.timeSource()) {
				if err := r.handle(request, handler, &v); err != nil {
					return err
				}
			}
		}
	}
	return nil
}

func (r *Redis) handle(request *stored, handler func(model.Request) error, member *redis.Z) error {
	lock, err := r.lock(request.ID)
	if err != nil {
		return err
	}
	defer func() {
		if err := r.unlock(request.ID, lock); err != nil {
			r.logger.Error("unable to release lock", "id", request.ID)
		}
	}()
	if err := handler(request.Request()); err == nil {
		request.store = r
		request.member = member
		if err := request.Ack(); err != nil {
			r.logger.Error("unable to acknowledge request", "id", request.ID)
			return err
		}
		return nil
	} else {
		r.logger.Error("error handling request", "error", err)
		return err
	}
}

func (r *Redis) Add(request model.Request) error {
	id := uuid.New().String()
	buff, err := json.Marshal(&stored{
		ID:  id,
		Req: request,
	})
	if err != nil {
		return err
	}
	_, err = r.client.ZAdd(context.Background(), "sloth:", redis.Z{
		Score:  float64(request.DelayedUntil.UnixNano()) / 1000000000,
		Member: string(buff),
	}).Result()
	return err
}

func (r *Redis) remove(member *redis.Z) error {
	_, err := r.client.ZRem(context.Background(), "sloth:", member.Member).Result()
	if err != nil {
		r.logger.Error("unable to remove request", "error", err)
	}
	return err
}

func (r *Redis) lock(id string) (string, error) {
	identifier := uuid.New().String()
	end := r.timeSource().Add(2 * time.Second)
	for r.timeSource().Before(end) {
		ok, err := r.client.SetNX(context.Background(), lockName(id), identifier, 0).Result()
		if err != nil {
			return "", err
		}
		if ok {
			return identifier, nil
		}
		r.sleep(10 * time.Millisecond)
	}
	return "", fmt.Errorf("unable to lock id '%s'", id)
}

func (r *Redis) unlock(id, _ string) error {
	ctx := context.Background()
	lockName := lockName(id)
	return r.client.Del(ctx, lockName).Err()
}

func lockName(id string) string {
	return fmt.Sprintf("lock:%s", id)
}

func (r *Redis) Stop() error {
	r.mutex.Lock()
	defer r.mutex.Unlock()
	r.logger.Info("stopping")
	if r.ctx != nil {
		r.cancel()
		return r.client.Close()
	}
	return nil
}

var _ model.Store = &Redis{}

type stored struct {
	store  *Redis
	member *redis.Z
	ID     string
	Req    model.Request
}

func (s stored) Request() model.Request {
	return s.Req
}

func (s stored) Ack() error {
	return s.store.remove(s.member)
}

func main() {
	logger := hclog.Default()
	plugin.Serve(&plugin.ServeConfig{
		HandshakeConfig: mp.Handshake,
		Plugins: map[string]plugin.Plugin{
			"store": &mp.StorePlugin{
				Impl: &Redis{logger: logger},
			},
		},
		GRPCServer: plugin.DefaultGRPCServer,
	})
}
